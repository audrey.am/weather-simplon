import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import "./villes.json";
import { useState } from "react";
import Info from "./Info";
import HautDePage from "./HautDePage";
import BasDePage from "./BasDePage";
import L from "leaflet";
const position = [43.564, 2.2806];
let city = require("./villes.json");
var redIcon = new L.Icon({
  iconUrl:
    "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png",
  shadowUrl:
    "https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png",
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41],
});

function Vue() {
  const [positionDuClient, changePosition] = useState([0, 0]);

  if ("geolocation" in navigator) {
    console.log("geo in nav");
    navigator.geolocation.getCurrentPosition(function (position) {
      changePosition([position.coords.latitude, position.coords.longitude]);
    });
  }

  const [i, setI] = useState(0);
  function changerIndex(index) {
    setI(index);
  }

  const [meteo, setMeteo] = useState({});

  function getMeteo(lat, long) {
    fetch(
      "https://api.openweathermap.org/data/2.5/weather?lat=" +
        lat +
        "&lon=" +
        long +
        "&appid=c6d3260a3b79f03aa265512b515f4427&units=metric&lang=fr"
    )
      .then((response) => response.json())
      .then((obj) => {
        setMeteo(obj);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  return (
    <div>
      <HautDePage></HautDePage>
      <h2>Simplon près de chez vous</h2>
      <div className="map-info">
        <div className="information">
          <hr />
          <div className="fabrique">
            <div className="textfabrique">
              <Info num={i}></Info>
            </div>
          </div>
        </div>

        <div id="map">
          <MapContainer center={position} zoom={7}>
            <TileLayer
              attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              url="https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}"
            />

            {city.map(({ ville, coordonnee }, index) => (
              <Marker
                icon={redIcon}
                position={[coordonnee.lat, coordonnee.long]}
                key={ville}
                eventHandlers={{
                  click: () => {
                    changerIndex(index);
                    getMeteo(coordonnee.lat, coordonnee.long);
                    
                  },
                }}
              >
                <Popup>
                  <h3>SIMPLON {[ville]}</h3>
                  <p>
                    <img
                      src={
                        meteo.weather
                          ? `https://openweathermap.org/img/wn/${meteo.weather[0].icon}@2x.png`
                          : ""
                      }
                      alt="weather icon"
                    />
                    <br />
                    Température {meteo.weather ? meteo.main.temp : ""} °C <br />
                    Pression {meteo.weather ? meteo.main.pressure : ""} Pa
                  </p>
                  <hr></hr>
                </Popup>
              </Marker>
            ))}
            <Marker position={positionDuClient}>
              <Popup>
                Vous êtes ici
                
                <p>
                  <img
                    src={
                      meteo.weather
                        ? `https://openweathermap.org/img/wn/${meteo.weather[0].icon}@2x.png`
                        : ""
                    }
                    alt="weather icon"
                  />
                  <br />
                  Température {meteo.weather ? meteo.main.temp : ""} °C <br />
                  Pression {meteo.weather ? meteo.main.pressure : ""} Pa
                  
                </p>
                <hr></hr>
              </Popup>
            </Marker>
          </MapContainer>
        </div>
        
      </div>
      <BasDePage></BasDePage>
    </div>
  );
}

export default Vue;
