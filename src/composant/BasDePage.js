function BasDePage() {
  return (
    <div className="fondrougebas">
      <img src="logo-footer.svg" alt="logo simplon"/>
      <div className="contact">
        <ul>
          <a href="https://simplon.co/mentions-legales.html">
            <li>Mentions légales |</li>
          </a>
          <a href="https://simplon.co/nos-actualites.html">
            <li>Nos actualités |</li>
          </a>
          <a href="https://simplon.co/nous-rejoindre.html">
            <li>Rejoindre Nos équipes |</li>
          </a>
          <a href="https://simplon.co/contact.html">
            <li>Contact |</li>
          </a>
          <a href="https://simplon.co/presse.html">
            <li>Presse |</li>
          </a>
          <a href="https://simplon.co/plan-du-site">
            <li>Plan du site |</li>
          </a>
          <a href="https://www.simplonprod.co/">
            <li>Notre agence numérique</li>
          </a>
        </ul>
      </div>
    </div>
  );
}
export default BasDePage;
