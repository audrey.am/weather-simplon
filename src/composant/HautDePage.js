function HautDePage() {
  return (
    <div>
      <div className="lien">
        <a href="https://occitanie.simplon.co/">SIMPLON.CO</a>
        <div className="lien-reseau">
          <a href="https://twitter.com/Simplon_Occ">
            <img src="twitter.svg" alt="logo twitter" />
          </a>
          <a href="https://www.linkedin.com/company/simplon-occitanie/">
            <img src="linkedin.svg" alt="logo linkedIn" />
          </a>
          <a href="https://www.facebook.com/simplon.occitanie">
            <img src="facebook.svg" alt="logo facebook" />
          </a>
        </div>
      </div>
      <div className="menu">
        <img src="telechargement.png" alt="logo simplon" />
        <ul>
          <div className="menudroit">
            <li>VOUS VOULEZ</li>
            <li>WELCODE</li>
            <li>NOS ACTUALITES</li>
          </div>
        </ul>
      </div>
      <div className="fondrougehaut">
        <div className="texte">
          <h1>Simplon.co en Occitanie</h1>
          <p>
            Simplon.co est un réseau de fabrique solidaires et inclusives qui
            proposent des formations gratuites aux métiers techniques du
            numérique en France et à l’étranger
          </p>
          <div className="btn">
            <a className="a_du_button" href="https://occitanie.simplon.co/je-candidate">
              FORMATIONS OUVERTES
            </a>
          </div>
        </div>
        <div className="photo">
          <img src="Header-Simplon-Occitaniecrop.jpg" alt="classe de Simploniens"/>
        </div>
      </div>
    </div>
  );
}

export default HautDePage;
