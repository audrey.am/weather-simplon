import villes from "./villes.json";

function Info(props) {
  return (
    <div>
      <h3>SIMPLON {villes[props.num].ville}</h3>
      <p>{villes[props.num].adresse}</p>
      <p>{villes[props.num].info}</p>
    </div>
  );
}

export default Info;
